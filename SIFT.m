function [Fs, Ds] = SIFT(img)

% Add the sift library to path if the sift mex file cannot be found
% Here we assume the sift_lib is placed at a predefined location 
% (relative path)
% if ~isequal(exist('vl_sift'), 3)
%     sift_lib_dir = fullfile('sift_lib', ['mex' lower(computer)]);
%     orig_path = addpath(sift_lib_dir);
%     % Restore the original path upon function completion 
%     temp = onCleanup(@()path(orig_path));
% end

img = im2single(img);
if (ndims(img) == 3)
    gray_img = rgb2gray(img);
else
    gray_img = img;
end
[Fs, Ds] = vl_sift(gray_img);