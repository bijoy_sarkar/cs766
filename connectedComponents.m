function connCompStruct = connectedComponents(img_struct)

no_img = size(img_struct,2);
considered = zeros(1,no_img);
conn_comp_count = 1;

for i = 1:no_img  
    if(size(img_struct{i}.matches,2) == 0) % if image i has no match
        considered(i) = 1;
        continue;
    end
    if(considered(i) == 1)
        continue;
    end
    neighbors_old = [img_struct{i}.matches.img_index];
    
    while(true)
        neighbors_new = neighbors_old;
        for j = 1:size(neighbors_old,2)
            if(considered(neighbors_old(j)) == 1)
                continue;
            end
            to_add_new_neighbor = [img_struct{neighbors_old(j)}.matches.img_index];
            % filtered the images that have already been considered as part
            % of other connected components.
            % Issue with this part. How to resolve it. 2 is a neighbor of
            % 9. So, it would need to be considered.
            % Discuss with Bijoy.
            neighbors_new = unique([neighbors_new, to_add_new_neighbor]);
            considered(neighbors_old(j)) = 1;
        end
        diff_neighbor = setdiff(neighbors_new, neighbors_old); % arguments to functin are order dependent.
        count_diff_neigbor = size(diff_neighbor,2);
%         [i,  count_diff_neigbor]
%         neighbors_old
%         neighbors_new
        neighbors_old = neighbors_new;
        if(count_diff_neigbor == 0)
            break;
        end
    end
    connCompStruct{conn_comp_count}.components = neighbors_old;
    conn_comp_count = conn_comp_count + 1;
    % to check if all images have been considered
    if(sum(considered) == no_img)
        break;
    end
end
    

end

    