function retStruct = stitchImagePair(image1, image2, H_3x3)

    w1 = size(image1,2);
    h1 = size(image1,1);
    w2 = size(image2,2);
    h2 = size(image2,1);
    image2_box = [1, 1; 1, h2; w2, 1; w2, h2];
    dest_image2_box = round(applyHomography(inv(H_3x3), image2_box));
    x_min = min([min(dest_image2_box(:,1)), 1]);
    y_min = min([min(dest_image2_box(:,2)), 1]);
    x_max = max([max(dest_image2_box(:,1)), w1]);
    y_max = max([max(dest_image2_box(:,2)), h1]);  
    x_offset = 1 - x_min;
    y_offset = 1 - y_min;
    
    % Homography to translate
    H_adjust = [1, 0 , -x_offset; 0, 1, -y_offset; 0, 0, 1];
    
    [image1_mask, base] = backwardWarpImg(im2double(image1), H_adjust, [x_max + x_offset, y_max + y_offset]);
    % H_3x3 will give transformation from image1 to image2
    % H_adjust is used to adjust for the x_offset / y_offset that the
    % image1 has gone through to fit in the bounding box
    [image2_mask, dest_img] = backwardWarpImg(im2double(image2), H_3x3* H_adjust, [x_max + x_offset, y_max + y_offset]);
    stiched_img = blendImagePair(base, image1_mask, dest_img, image2_mask, 'blend');
    
    retStruct.stitchedImg = stiched_img;
    retStruct.H_image1 = inv(H_adjust);
    retStruct.H_image2 = inv(H_adjust) * inv(H_3x3);