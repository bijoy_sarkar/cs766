function out_img = blendImagePair(wrapped_imgs, masks, wrapped_imgd, maskd, mode)

if (strcmp(mode,'blend'))

  new_masks_inverted = ~masks;
  new_masks_weighted = bwdist(new_masks_inverted);
  new_masks_norm = new_masks_weighted ./ max(new_masks_weighted(:));
  new_masks_3 = cat(3, new_masks_norm, new_masks_norm, new_masks_norm);

  new_maskd_inverted = ~maskd;
  new_maskd_weighted = bwdist(new_maskd_inverted);
  new_maskd_norm = new_maskd_weighted ./ max(new_maskd_weighted(:));
  new_maskd_3 = cat(3, new_maskd_norm, new_maskd_norm, new_maskd_norm);
    
  out_img = (new_maskd_3.*im2double(wrapped_imgd) + ... 
      new_masks_3 .* im2double(wrapped_imgs)) ./ + ...
      (new_maskd_3 + new_masks_3);
  
elseif (strcmp(mode,'overlay'))
  maskd_logical = maskd > 0;
  out_img = im2double(wrapped_imgs) .* cat(3, ~maskd_logical, ...
      ~maskd_logical, ~maskd_logical) + im2double(wrapped_imgd) .* ...
      cat(3, maskd_logical, maskd_logical, maskd_logical);
end 

end