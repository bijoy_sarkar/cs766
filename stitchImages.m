function stitched_images = stitchImages(img_struct)

    pairs = [];
    for i=1:size(img_struct, 2)
        matches = img_struct{i}.matches;
        for k=1:size(matches, 2)
            j = matches(k).img_index;
            pairs = [pairs; i, j, k, matches(k).n_i];
        end
    end
    pairs = sortrows(pairs, -4)
    count = 0;
%     for i=1:size(pairs, 1)
%         if pairs(i, 4) > 1200
%             count = count + 1;
%             match = img_struct{pairs(i, 1)}.matches(pairs(i, 3));
%             stitched_image = stitchImagePair(img_struct{pairs(i, 1)}.img, img_struct{pairs(i, 2)}.img, match.homography);
%             stitched_images{count}.image = stitched_image;
%         end
%     end