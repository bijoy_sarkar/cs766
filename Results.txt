Current Data Set:

7 panoramic images with 4 images each. 3 random images. Total 31 images.

======================================================

tic
img_struct = FeatureMatching(img_struct, 4, 6);
toc

Elapsed time is 2007.844361 seconds.

======================================================

tic
img_struct = ImageMatching(img_struct, 500, 5);
toc

Elapsed time is 362.654237 seconds.

======================================================

tic
%gets us the group of images which are to be stitched.
connComps = connectedComponents(img_struct);
noOfConnComp = size(connComps,2);
for i=1:noOfConnComp
    connComps{i}.components
end
noOfConnComp = size(connComps,2);
for i=1:noOfConnComp
%for i=1:1
    stitchedImg = stitchTheComponent(img_struct, connComps{i}.components);
    stitchedImgStruct{i} = stitchedImg;
end
toc

Elapsed time is 196.885625 seconds.

======================================================
