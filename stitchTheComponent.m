function stitchedImg = stitchTheComponent(img_struct, connComps)
% img_struct is the same struct which is being used everywhere.
% connComps is the array containing the indices of the images to be
% stitched together.
    
% to define the stitching order
imgPairs = [];
for i=1:size(connComps, 2)
    matches = img_struct{connComps(i)}.matches;
    for k=1:size(matches, 2)
        j = matches(k).img_index;
        imgPairs = [imgPairs; connComps(i), j, k, matches(k).n_i];
    end
end
imgPairs = sortrows(imgPairs, -4);

% Initilize the individual images as one component with homography to be
% identity matrix
noCompToStitch = size(connComps, 2);
for i=1:noCompToStitch
    componentsToStitch{i}.stitchedImg = img_struct{connComps(i)}.img;
    componentsToStitch{i}.list{1}.homography = eye(3,3);
    componentsToStitch{i}.list{1}.index = connComps(i);
end

% Stitch the components
scannedImgPairIndex = 1; % keeps the current index in the array "imgPairs" 
% till where we have already scanned.
while(noCompToStitch > 1)
    % find the image pairs to be stitched and also find the corresponding 
    % components of the image pair to be stitched.
    while(true) 
        [comp_i, homography_of_img_i_in_comp_i] = ...
            findComponentofImage(componentsToStitch, imgPairs(scannedImgPairIndex,1));
        [comp_j, homography_of_img_j_in_comp_j] = ...
            findComponentofImage(componentsToStitch, imgPairs(scannedImgPairIndex,2));
        %[comp_i, comp_j]
        if(comp_i~=-1 && comp_j~=-1 && comp_i ~= comp_j)
            break;
        end
        scannedImgPairIndex = scannedImgPairIndex + 1;
    end
    
    %homography_of_img_j_in_comp_j = homography_of_img_j_in_comp_j / homography_of_img_j_in_comp_j(3,3)
    
    % stitch the corresponding components
    homography_btwn_i_and_j = ...
        img_struct{imgPairs(scannedImgPairIndex,1)}.matches( ...
        imgPairs(scannedImgPairIndex,3)).homography;
    
%    homography_between_comp_i_and_comp_j = homography_of_img_i_in_comp_i * ...
%        homography_btwn_i_and_j * inv(homography_of_img_j_in_comp_j);
 
    homography_between_comp_i_and_comp_j = homography_of_img_j_in_comp_j * ...
        homography_btwn_i_and_j * inv(homography_of_img_i_in_comp_i);

%     '====================================='
%     
%     [comp_i, comp_j]
%     imgPairs(scannedImgPairIndex,:)
%     homography_of_img_i_in_comp_i
%     homography_btwn_i_and_j
%     homography_of_img_j_in_comp_j
%     inv(homography_of_img_j_in_comp_j)
%     
%     '+++++++++++++++++++++++++++++++++++++++'
   
    %figure, 
    %subplot(121),imshow(componentsToStitch{comp_i}.stitchedImg);
    %subplot(122),imshow(componentsToStitch{comp_j}.stitchedImg);
    
    struct_stitch = ...
        stitchImagePair(componentsToStitch{comp_i}.stitchedImg, ...
        componentsToStitch{comp_j}.stitchedImg, ...
        homography_between_comp_i_and_comp_j);
    
    stitchedImg = struct_stitch.stitchedImg;
    H_comp_i = struct_stitch.H_image1;
    H_comp_j = struct_stitch.H_image2;
    
    
    % Merge the components
    % Adjusting component_i first
    componentsToStitch{comp_i}.stitchedImg = stitchedImg;
    %figure, imshow(componentsToStitch{comp_i}.stitchedImg);
    for i=1:size(componentsToStitch{comp_i}.list,2)
        componentsToStitch{comp_i}.list{i}.homography = ...
            H_comp_i * componentsToStitch{comp_i}.list{i}.homography;
    end
    % Adjusting elements of component_j and adding them to component_i
    for j=1:size(componentsToStitch{comp_j}.list,2)
        componentsToStitch{comp_i}.list{i+j}.homography = ...
            H_comp_j * componentsToStitch{comp_j}.list{j}.homography;
        componentsToStitch{comp_i}.list{i+j}.index = ...
            componentsToStitch{comp_j}.list{j}.index;
    end
    
    componentsToStitch = {componentsToStitch{1:comp_j-1}, ...
        componentsToStitch{comp_j+1:end}};
    
    noCompToStitch = noCompToStitch - 1;
end

stitchedImg = componentsToStitch{1}.stitchedImg;

end % end of stitchTheComponent function


% function compIndexArr = findComponentIndexOfAllImages(componentsToStitch, connComps)
% 
% noImgs = size(connComps,2);
% compIndexArr = zeros(noImgs, 2);
% for i = 1:noImgs
%    compIndexArr(i,1) = connComps(i);
%    compIndexArr(i,2) = findImageInComponent(componentsToStitch, connComps(i));
% end
%
%end % end of function "findComponentIndexOfAllImages"

function [compIndex, homography] = findComponentofImage(componentsToStitch, imgIndex)

compIndex = -1;
homography = zeros(3,3);
noComp = size(componentsToStitch, 2);
% for i=1:noComp
%     %imgIndicesInThisComp = [componentsToStitch{i}.list{1:end}.index];
%     struct_list_of_this_comp = componentsToStitch{i}.list;
%     imgIndicesInThisComp = zeros(1,size(struct_list_of_this_comp,2));
%     for j=1:size(struct_list_of_this_comp,2)
%         imgIndicesInThisComp(j) = struct_list_of_this_comp{j}.index;
%     end
%     for j=1:size(imgIndicesInThisComp,2)
%         if(imgIndicesInThisComp(j) == imgIndex)
%             compIndex = i;
%             return;
%         end
%     end
% end


for i=1:noComp
    %imgIndicesInThisComp = [componentsToStitch{i}.list{1:end}.index];
    struct_list_of_this_comp = componentsToStitch{i}.list;
    for j=1:size(struct_list_of_this_comp,2)
        if (imgIndex == struct_list_of_this_comp{j}.index)
            compIndex = i;
            homography = struct_list_of_this_comp{j}.homography;
            return;
        end
    end
end

end % end of function "findImageInComponent" 
