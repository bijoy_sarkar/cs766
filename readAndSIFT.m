function img_struct = readAndSIFT(path)
%Read the images from the path and calculate SIFT features for each of 
%the images
% OUTPUT : A struct containing all the images all with their SIFT features.

imagefiles = dir(fullfile(path, '*.png'));

%img_struct = struct('img','','sift_feature','','sift_descriptor','');

nfiles = length(imagefiles);    % Number of files found
for i=1:nfiles
   currentfilename = imagefiles(i).name;
   currentimage = imread(fullfile(path, currentfilename));
   img_struct{i}.img = currentimage;
   % Calculate SIFT features for the image
   [img_struct{i}.sift_feature, img_struct{i}.sift_descriptor] = ...
       SIFT(currentimage);
end


end