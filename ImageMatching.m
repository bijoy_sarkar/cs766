function img_struct = ImageMatching(img_struct, ransac_n, eps)
    nfiles = size(img_struct,2);
    for i=1:nfiles
        current_img = img_struct{i};
        img_struct{i}.matches = [];
        possible_matches = current_img.image_neigbors;
        
        max_matches = 0;
        %img_struct{i}.matches = [];
        matches = [];
        for j = 1: size(possible_matches, 2)
            neighbor = img_struct{possible_matches(j)};
            [Xs, Xd] = genSIFTMatches(current_img.img, neighbor.img);
            [inliers_id, H_3x3] = runRANSAC(Xs, Xd, ransac_n, eps);
            n_i = size(inliers_id, 1);
            n_f = size(Xs, 1);
            max_matches = max(max_matches, n_f);
            %[i, j , n_i, n_f]
            if(n_i > (8 + 0.5 * n_f))
                match_struct.img_index = possible_matches(j);
                match_struct.homography = H_3x3;
                match_struct.Xs = Xs;
                match_struct.Xd = Xd;
                match_struct.n_i = n_i;
                match_struct.n_f = n_f;
                matches = [matches, match_struct];
                %img_struct{i}.matches = [img_struct{i}.matches, match_struct];
            end
            %B_coefficient = nchoosek(n_f, n_i);
            %p_1 = 0.7; 
            %p_0 = 0.01;
            %B_p1 = B_coefficient * (p_1^n_i) * ((1-p_1)^(n_f-n_i));
            %B_p0 = B_coefficient * (p_0^n_i) * ((1-p_0)^(n_f-n_i));
            %if((B_p1/B_p0) > (1 / (1/0.97 - 1)))
            %    [i, j]
            %end
        end
        
        limit = max_matches/4;
        filtered_match = [];
        for j = 1: size(matches,2)
            if (matches(j).n_f >= limit)
                filtered_match = [filtered_match, matches(j)];
            end
        end
        img_struct{i}.matches = filtered_match;
    end 
end