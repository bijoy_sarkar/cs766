function [inliers_id, H] = runRANSAC(Xs, Xd, ransac_n, eps)

inliers_id = [];
H = zeros(3);

for i = 1:ransac_n
    random_point_indices = randsample(size(Xs, 1),4);
    curr_H = computeHomography(Xs(random_point_indices,:), Xd(random_point_indices,:));
    Xd_pred = applyHomography(curr_H, Xs);
    error = sqrt(sum((Xd_pred-Xd).^2, 2));
    model_inliers = find(error<=eps);
    if(size(model_inliers, 1) > size(inliers_id, 1))
        inliers_id = model_inliers;
        H = curr_H;
    end
end

end