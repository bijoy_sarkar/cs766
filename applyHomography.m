function dest_pts_nx2 = applyHomography(H_3x3, src_pts_nx2)

format long
q = H_3x3 * [src_pts_nx2'; ones(1, size(src_pts_nx2, 1))];
p = q(3,:);
dest_pts_nx2 = [q(1,:)./p; q(2,:)./p]';