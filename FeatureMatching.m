function img_struct = FeatureMatching(img_struct, feature_neighbors, image_neighbors)
%INPUT DESCRIPTION:
%img_struct : structure containign all the images along with SIFT feature
%and SIFT descriptor.
%feature_neighbors : no of feature neighbor to be calculated for each feature.
%image_neighbors : no of image neighbor to be calculated for each image.

  %Get all the descriptors from all the images together for k-d tree.
  nfiles = size(img_struct,2);
  all_desc = [];
  index_of_desc = zeros(1,nfiles);
  for i=1:nfiles
    all_desc = [all_desc, img_struct{i}.sift_descriptor];
    if (i == 1)
      index_of_desc(i) = size(img_struct{i}.sift_descriptor, 2);
    else 
      index_of_desc(i) = index_of_desc(i-1) ...
                + size(img_struct{i}.sift_descriptor, 2);
    end
  end

  %k_dTreeModel = KDTreeSearcher(all_desc', 'Distance', 'euclidean');
  k_dTreeModel = vl_kdtreebuild(double(all_desc));
  for i=1:nfiles
    %Feature matching is being done here.
    [index, dist] = vl_kdtreequery(k_dTreeModel, double(all_desc), ...
       double(img_struct{i}.sift_descriptor), 'NUMNEIGHBORS', ...
       feature_neighbors + 1);
    %Ground preparation for Image Matching
    count = zeros(1,nfiles);
    prev = 0;
    index = index(2:feature_neighbors+1,:);%remove first row because it is identical to column number
    dist = dist(2:feature_neighbors+1,:);%remove first row because it is all zero
    for j=1:nfiles
      next = index_of_desc(j);
      found = (index>prev & index<=next);
      count(j) = sum(sum(found));
      prev = next;
    end
    % Image Matching is being done here.
    count(i) = 0; % This is self image and it shouldnt be taken as neighbor for itself.
    [count_sorted,I] = sort(count,'descend');
    img_struct{i}.image_neigbors = I(1,1:image_neighbors);
  end

end