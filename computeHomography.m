function H_3x3 = computeHomography(src_pts_nx2, dest_pts_nx2)

l = size(src_pts_nx2, 1);

p = [src_pts_nx2, ones(l,1)];

l1 = [src_pts_nx2, ones(l,1), zeros(l,3), (p .* (-repmat(dest_pts_nx2(:,1), 1, 3)))];
l2 = [zeros(l,3), src_pts_nx2, ones(l,1), (p .* (-repmat(dest_pts_nx2(:,2), 1, 3)))];
%A = [l1; l2];

format long

A = zeros(l+l,9);
A(1:2:end,:) = l1;
A(2:2:end,:) = l2;

[V, D] = eig(A' * A);
[m, i] = min(diag(D));
eigenvector = V(:,i);
%norm = sqrt(eigenvector' * eigenvector)

H_3x3 = [eigenvector(1:3)';eigenvector(4:6)';eigenvector(7:9)'];