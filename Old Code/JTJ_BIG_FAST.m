function JTJ_BIG_mat = JTJ_BIG_FAST(img_struct, params)

no_img = size(img_struct, 2);
JTJ_BIG_mat = zeros(4*no_img, 4*no_img);

for i = 1:no_img
    no_img_match = size(img_struct{i}.matches,2);
    for j=1:no_img_match
        corres_img_index = img_struct{i}.matches(j).img_index;
        JTJ_BIG_mat(4*i-3:4*i, 4*corres_img_index-3:4*corres_img_index) = JTJ_FAST(img_struct{i}.matches(j), params, i, corres_img_index);
    end
end
end