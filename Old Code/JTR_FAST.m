function JTR_mat = JTR_FAST(matching_features, param_i, param_j)
% Pass the img_struct{i}.matches(j) to this method.
% param_i is of the form [theta_i_1; theta_i_2; theta_i_3; f_i]; (4x1)
% param_j is of the form [theta_j_1; theta_j_2; theta_j_3; f_j]; (4x1)

%param_i = [pi/2; pi/4; pi/8; 10];
%param_j = [pi/6; 0; pi/4; 15];

K_i = [param_i(4), 0, 0; 0, param_i(4), 0; 0, 0, 1];
K_j = [param_j(4), 0, 0; 0, param_j(4), 0; 0, 0, 1];
R_i = expm([0, -param_i(3), param_i(2); ...
            param_i(3), 0, -param_i(1); ...
           -param_i(2), param_i(1), 0] ...
           );
R_j = expm([0, -param_j(3), param_j(2); ...
            param_j(3), 0, -param_j(1); ...
           -param_j(2), param_j(1), 0] ...
           );
H =  K_i * R_i * R_j' / (K_j);

u_i = matching_features.Xs; % Nx2 matrix

u_j = [matching_features.Xd'; ones(1, size(matching_features.Xd, 1))]; % 3 x N matrix
p_ij_3xn = H * u_j;
p_ij_nx3 = p_ij_3xn';

p_ij_nx2 = [p_ij_nx3(:,1)./p_ij_nx3(:,3), p_ij_nx3(:,2)./p_ij_nx3(:,3)];

residual_ij = u_i - p_ij_nx2; % Nx2 matrix

z_reciprocal = 1 ./ p_ij_nx3(:,3);
x_z2 =  -p_ij_nx3(:,1) .* (z_reciprocal.^2);
y_z2 =  -p_ij_nx3(:,2) .* (z_reciprocal.^2);

del_p_part1 = zeros(2*size(matching_features.Xd,1),3);
del_p_part1(1:2:end,1) = z_reciprocal;
del_p_part1(2:2:end,2) = z_reciprocal;
del_p_part1(1:2:end,3) = x_z2;
del_p_part1(2:2:end,3) = y_z2;

del_p_wrt_theta_i1 = K_i * R_i * [0,0,0; 0,0,-1; 0,1,0] * R_j' * inv(K_j) * u_j;
del_p_wrt_theta_i2 = K_i * R_i * [0,0,1; 0,0,0; -1,0,0] * R_j' * inv(K_j) * u_j;
del_p_wrt_theta_i3 = K_i * R_i * [0,-1,0; 1,0,0; 0,0,0] * R_j' * inv(K_j) * u_j;
del_p_wrt_f_i = [1,0,0; 0,1,0; 0,0,0] * R_i * R_j' * inv(K_j) * u_j;

del_p_final_wrt_theta_i1 = matric_mult_helper(del_p_part1, del_p_wrt_theta_i1);
del_p_final_wrt_theta_i2 = matric_mult_helper(del_p_part1, del_p_wrt_theta_i2);
del_p_final_wrt_theta_i3 = matric_mult_helper(del_p_part1, del_p_wrt_theta_i3);
del_p_final_wrt_f_i = matric_mult_helper(del_p_part1, del_p_wrt_f_i);

JTR_mat = zeros(4,1);

for idx=1:size(matching_features.Xd,1)
    del_r_ij_wrt_phi_i_2x4 = [del_p_final_wrt_theta_i1(idx,:)', ...
                              del_p_final_wrt_theta_i2(idx,:)', ...
                              del_p_final_wrt_theta_i3(idx,:)', ...
                              del_p_final_wrt_f_i(idx,:)', ...
                             ];
    
    JTR_mat = JTR_mat + del_r_ij_wrt_phi_i_2x4' * residual_ij(idx,:)';
end

end

function result = matric_mult_helper(del_p_part1, del_prop)
% Input: del_p_part1 is a 2kx3 matrix. 
%       del_prop is a 3xk marix. 
% Want: In del_p_part1, each consecutive 2 rows is derived from a particular
% point. Fofr the same point we have a row in the del_prop matrix.
% Basically, we want to do the multiplication between these two matrics
% corresponding to same point for each of the k points.

% OUTPUT: result is the desired output matrix with dimesnion kx2.

del_prop_dup_2kx3 = zeros(size(del_p_part1));
del_prop_dup_2kx3(1:2:end,:) = del_prop';
del_prop_dup_2kx3(2:2:end,:) = del_prop';

mult_pointwise = del_p_part1 .* del_prop_dup_2kx3;

points_2kx1 = sum(mult_pointwise,2);

result = zeros(size(del_prop,2),2);
result(:,1) = points_2kx1(1:2:end);
result(:,2) = points_2kx1(2:2:end);
end
