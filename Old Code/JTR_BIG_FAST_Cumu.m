function JTR_BIG_mat = JTR_BIG_FAST_Cumu(img_struct, params)

no_img = size(img_struct, 2);
JTR_BIG_mat = zeros(4*no_img, 1);

for i = 1:no_img
    JTR_BIG_mat(4*i-3:4*i, 1) = JTR_FAST_Cumu(img_struct{i}.matches, params, i);
end

end