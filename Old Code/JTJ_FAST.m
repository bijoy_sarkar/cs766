function JTJ_mat = JTJ_FAST(matching_features, param, i, j)
% Pass the img_struct{i}.matches(j) to this method as "matching_features".
% param is 4I X 1 matrix. I is the total number of images being considered
% for stitching.
% i and j are the images for which we are calculating J'J.

%param_i = [pi/2; pi/4; pi/8; 10];
%param_j = [pi/6; 0; pi/4; 15];
% param_i is of the form [theta_i_1; theta_i_2; theta_i_3; f_i]; (4x1)
% param_j is of the form [theta_j_1; theta_j_2; theta_j_3; f_j]; (4x1)
param_i = param(4*i-3:4*i,1);
param_j = param(4*j-3:4*j,1);

K_i = [param_i(4), 0, 0; 0, param_i(4), 0; 0, 0, 1];
K_j = [param_j(4), 0, 0; 0, param_j(4), 0; 0, 0, 1];
R_i = expm([0, -param_i(3), param_i(2); ...
            param_i(3), 0, -param_i(1); ...
           -param_i(2), param_i(1), 0] ...
           );
R_j = expm([0, -param_j(3), param_j(2); ...
            param_j(3), 0, -param_j(1); ...
           -param_j(2), param_j(1), 0] ...
           );
H =  K_i * R_i * R_j' / (K_j);

u_j = [matching_features.Xd'; ones(1, size(matching_features.Xd, 1))];
p_ij_3xn = H * u_j;
p_ij_nx3 = p_ij_3xn';

z_reciprocal = 1 ./ p_ij_nx3(:,3);
x_z2 =  -p_ij_nx3(:,1) .* (z_reciprocal.^2);
y_z2 =  -p_ij_nx3(:,2) .* (z_reciprocal.^2);

del_p_part1 = zeros(2*size(matching_features.Xd,1),3);
del_p_part1(1:2:end,1) = z_reciprocal;
del_p_part1(2:2:end,2) = z_reciprocal;
del_p_part1(1:2:end,3) = x_z2;
del_p_part1(2:2:end,3) = y_z2;

del_p_wrt_theta_i1 = K_i * R_i * [0,0,0; 0,0,-1; 0,1,0] * R_j' * inv(K_j) * u_j;
del_p_wrt_theta_i2 = K_i * R_i * [0,0,1; 0,0,0; -1,0,0] * R_j' * inv(K_j) * u_j;
del_p_wrt_theta_i3 = K_i * R_i * [0,-1,0; 1,0,0; 0,0,0] * R_j' * inv(K_j) * u_j;
del_p_wrt_f_i = [1,0,0; 0,1,0; 0,0,0] * R_i * R_j' * inv(K_j) * u_j;

del_p_final_wrt_theta_i1 = matric_mult_helper(del_p_part1, del_p_wrt_theta_i1);
del_p_final_wrt_theta_i2 = matric_mult_helper(del_p_part1, del_p_wrt_theta_i2);
del_p_final_wrt_theta_i3 = matric_mult_helper(del_p_part1, del_p_wrt_theta_i3);
del_p_final_wrt_f_i = matric_mult_helper(del_p_part1, del_p_wrt_f_i);

del_p_wrt_theta_j1 = K_i * R_i * R_j' * [0,0,0; 0,0,1; 0,-1,0] * inv(K_j) * u_j;
del_p_wrt_theta_j2 = K_i * R_i * R_j' * [0,0,-1; 0,0,0; 1,0,0] * inv(K_j) * u_j;
del_p_wrt_theta_j3 = K_i * R_i * R_j' * [0,1,0; -1,0,0; 0,0,0] * inv(K_j) * u_j;
f_j = param_j(4);
del_p_wrt_f_j = (1/f_j).^2 .* (K_i * R_i * R_j' * [-1,0,0; 0,-1,0; 0,0,0] * u_j);

del_p_final_wrt_theta_j1 = matric_mult_helper(del_p_part1, del_p_wrt_theta_j1);
del_p_final_wrt_theta_j2 = matric_mult_helper(del_p_part1, del_p_wrt_theta_j2);
del_p_final_wrt_theta_j3 = matric_mult_helper(del_p_part1, del_p_wrt_theta_j3);
del_p_final_wrt_f_j = matric_mult_helper(del_p_part1, del_p_wrt_f_j);

JTJ_mat = zeros(4,4);

for idx=1:size(matching_features.Xd,1)
    del_r_ij_wrt_phi_i_2x4 = [del_p_final_wrt_theta_i1(idx,:)', ...
                              del_p_final_wrt_theta_i2(idx,:)', ...
                              del_p_final_wrt_theta_i3(idx,:)', ...
                              del_p_final_wrt_f_i(idx,:)', ...
                             ];
    
    del_r_ij_wrt_phi_j_2x4 = [del_p_final_wrt_theta_j1(idx,:)', ...
                              del_p_final_wrt_theta_j2(idx,:)', ...
                              del_p_final_wrt_theta_j3(idx,:)', ...
                              del_p_final_wrt_f_j(idx,:)', ...
                             ];
    
    JTJ_mat = JTJ_mat + del_r_ij_wrt_phi_i_2x4' * del_r_ij_wrt_phi_j_2x4;
end



end

function result = matric_mult_helper(del_p_part1, del_prop)
% Input: del_p_part1 is a 2kx3 matrix. 
%       del_prop is a 3xk marix. 
% Want: In del_p_part1, each consecutive 2 rows is derived from a particular
% point. Fofr the same point we have a row in the del_prop matrix.
% Basically, we want to do the multiplication between these two matrics
% corresponding to same point for each of the k points.

% OUTPUT: result is the desired output matrix with dimesnion kx2.

del_prop_dup_2kx3 = zeros(size(del_p_part1));
del_prop_dup_2kx3(1:2:end,:) = del_prop';
del_prop_dup_2kx3(2:2:end,:) = del_prop';

mult_pointwise = del_p_part1 .* del_prop_dup_2kx3;

points_2kx1 = sum(mult_pointwise,2);

result = zeros(size(del_prop,2),2);
result(:,1) = points_2kx1(1:2:end);
result(:,2) = points_2kx1(2:2:end);
end
