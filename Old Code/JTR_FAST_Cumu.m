function JTR_final = JTR_FAST_Cumu(matching_features, params, current_img_idx)
% INPUT DESCRIPTION :
% matching features are the matches for current_img_idx
% params is 4I X 1 matrix. I is the total number of images being considered
% for stitching.
% current_img_idx is the image for which we are calculating sum of J'R for
% all of its matching images.

% OUTPUT DESCRIPTION:
% JTR_final is 4 X 1 matrix containing J'R wrt current_img_idx.

no_match = size(matching_features,2);
JTR_final = zeros(4,1);

for i=1:no_match
    if(i == current_img_idx)
        continue;
    end
    corres_img_idx = matching_features(i).img_index;
    JTR_final = JTR_final + JTR_FAST(matching_features(i), params(4*current_img_idx-3:4*current_img_idx,1), params(4*corres_img_idx-3:4*corres_img_idx,1));
end

end