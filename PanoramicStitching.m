% Setup VL FEAT Library
run '/afs/cs.wisc.edu/u/b/s/bsarkar/766/cs766/vlfeat-0.9.20/toolbox/vl_setup'

tic
img_struct = readAndSIFT('/afs/cs.wisc.edu/u/b/s/bsarkar/766/cs766/data_input');
toc

tic
img_struct = FeatureMatching(img_struct, 4, 6);
toc

tic
img_struct = ImageMatching(img_struct, 500, 5);
toc

tic
%gets us the group of images which are to be stitched.
connComps = connectedComponents(img_struct);
noOfConnComp = size(connComps,2);
for i=1:noOfConnComp
    connComps{i}.components
end
noOfConnComp = size(connComps,2);
for i=1:noOfConnComp
%for i=1:1
    stitchedImg = stitchTheComponent(img_struct, connComps{i}.components);
    stitchedImgStruct{i} = stitchedImg;
end
toc

for i=1:noOfConnComp
    figure, imshow(stitchedImgStruct{i});
end


% connComps = [9,10,11,12];
% connComps = [5,6,7,8];
% stitchedImg = stitchTheComponent(img_struct, connComps);


%stitched_images = stitchImages(img_struct);

%stiched_image = stitchImagePair(img_struct{10}.img, img_struct{9}.img, img_struct{10}.matches(1).homography);

% itr = 5;
% tolrance = 0.001;
% 
% no_img = size(img_struct,2);
% 
% params_old = ones(4*no_img,1);
% diag_elem = zeros(no_img*4,1);
% diag_elem(:,1) = pi/16;
% focal_avg = mean(params_old(4:4:end));
% diag_elem(4:4:end,1) = focal_avg;
% cov_mat = diag(diag_elem);
% 
% lambda = 1;
% 
% for i=1:itr
%     JTJ_BIG_mat = JTJ_BIG_FAST(img_struct, params_old);
%     JTR_BIG_mat = JTR_BIG_FAST_Cumu(img_struct, params_old);
%     params_new = inv(JTJ_BIG_mat + lambda.*inv(cov_mat))*(-JTR_BIG_mat);
%     
%     diag_elem = zeros(no_img*4,1);
%     diag_elem(:,1) = pi/16;
%     focal_avg = mean(params_new(4:4:end));
%     diag_elem(4:4:end,1) = focal_avg;
%     cov_mat = diag(diag_elem);
%     
%     diff_params = abs(params_new - params_old);
%     sum(diff_params)
%     
%     params_old = params_new;
% end

% 
% zzz = JTJ_FAST(img_struct{1}.matches(1), [pi/2; pi/4; pi/8; 10], [pi/6; 0; pi/4; 15]);
% zzz1 = JTR_FAST(img_struct{1}.matches(1), [pi/2; pi/4; pi/8; 10], [pi/6; 0; pi/4; 15]);
% 
% yyy = JTR_FAST(img_struct{1}, [pi/2; pi/4; pi/8; 10]);
