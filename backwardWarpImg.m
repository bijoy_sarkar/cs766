function [mask, result_img] = backwardWarpImg(src_img, resultToSrc_H,...
    dest_canvas_width_height)

% src_img is the image that needs to be reverse warped

% resultToSrc_H is the inverse homography so given a destination point (in
% bg_img) we can find the corresponding source img (in src_img)
% dest_canvas_width_height is the width and heigth of destination canvas
% H_3x3 portrait_pts -> bg_pts
% resultToSrc_H bg_pts -> portrait_pts
width = dest_canvas_width_height(1);
height = dest_canvas_width_height(2);

% For each point in (1,1) to (height, width) find the corresponding point
% If it is within src_img size then that bit of mask is set to true and
% pixel copied to result_img


mask = false(height, width);
result_img = zeros([height, width, 3]);

x_limit = size(src_img, 2);
y_limit = size(src_img, 1);

for i=1:height
    
    % pos = [x1, x2, x3, ......; y1, y2, y3, ......;1, 1, 1]
    pos = resultToSrc_H * [1:1:width; i * ones(1, width); ones(1, width)];
    z = pos(3,:);
    src_pts = [pos(1,:)./z; pos(2,:)./z];
    
    for j = 1:width
        x = round(src_pts(1,j));
        y = round(src_pts(2,j));
        if(( x >=1 && x <=x_limit ) && ( y >=1 && y <=y_limit ) && (max(src_img(y,x,:)>0)))
            mask(i,j) = true;
            result_img(i,j,:) = src_img(y,x,:);
        end
    end
end


